#!/usr/bin/env python


# in:  48.54972917,9.1188555,350.84,0.34,5,2150:572457.150,15:00:39.600 03/27/2021
# out: 46.753014 23.584244, 214.936523, 4.813889, 10.000000, 1461049218797807, 19.04.2016 09:00:18


def week_seconds_to_utc(gps_week, gps_seconds, leap_seconds):
    import datetime
    datetime_format = "%d.%m.%Y %H:%M:%S"
    epoch = datetime.datetime.strptime("06.01.1980 00:00:00", datetime_format)
    elapsed = datetime.timedelta(days=(gps_week * 7), seconds=(gps_seconds - leap_seconds))
    return datetime.datetime.strftime(epoch + elapsed, datetime_format)


def to_telenav_csv(gps_data, last_heading):
    tokens = gps_data.split(",")
    if len(tokens) != 7:
        return "0", ""
    if not tokens[0]:
        return "0", ""
    if not tokens[1]:
        return "0", ""
    if not tokens[5]:
        return "0", ""

    latitude = tokens[0]
    longitude = tokens[1]

    heading = last_heading
    if tokens[2]:
        heading = tokens[2]

    speed = "0"
    if tokens[3]:
        speed = tokens[3]

    accuracy = "5"
    if tokens[4]:
        accuracy = tokens[4]

    gps_time = tokens[5].split(":")

    # let's just go with this for now, it's enough for the nav-simulator, micro secs
    timestamp = int(float(gps_time[1]) * 1000 * 1000)

    # datetime
    # GPS time was zero at 0h 6-Jan-1980 and since it is not perturbed by leap seconds
    # GPS is now ahead of UTC by 18 seconds
    datetime = week_seconds_to_utc(int(gps_time[0]), int(float(gps_time[1]) * 1000) / 1000, -18)

    line = latitude + " " + longitude + "," + heading + "," + speed + "," + accuracy \
        + "," + str(timestamp) + "," + datetime + "\n"
    return heading, line


if __name__ == '__main__':
    print("\nFormat GPS positions from the UBLOX log")

    with open('./data/Ublox.csv', 'r') as fIn:
        with open('./data/UbloxToTelenav.csv', 'w') as fOut:
            current_heading = "0"
            for inLine in fIn:
                current_heading, outLine = to_telenav_csv(inLine, current_heading)
                if outLine:
                    fOut.write(outLine)
                else:
                    print("Invalid line or no data\n")
            print("Done")
