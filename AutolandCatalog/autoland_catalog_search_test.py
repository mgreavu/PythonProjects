import psycopg2
import multiprocessing
import sys
import timeit


g_host = "localhost"
g_user = "postgres"
g_passw = "test"

# https://www.postgresql.org/docs/9.2/static/monitoring-stats.html


def db_stats():
    try:
        conn = psycopg2.connect(host=g_host, database="autoland_catalog", user=g_user, password=g_passw)
        
        queries_list = ["SELECT table_name, pg_relation_size(table_name) AS table_size FROM information_schema.tables WHERE table_schema = 'public' ORDER by 2 DESC",
                        "SELECT * FROM pg_stat_user_tables ORDER BY n_live_tup DESC",
                        "SELECT * FROM pg_stat_database",
                        "SELECT * FROM pg_statio_user_tables"]
        fntx = 0
        for query in queries_list:
            cursor = conn.cursor()
            cursor.execute(query)

            result = ""
            colnames = [desc[0] for desc in cursor.description]
            n_cols = len(colnames)
            for col in range(n_cols):
                result += colnames[col]
                if col < n_cols - 1:
                    result += ","
            result += "\n"

            rows = cursor.fetchall()
            for row in rows:
                for col in range(n_cols):
                    result += str(row[col])
                    if col < n_cols - 1:
                        result += ","
                result += "\n"

            cursor.close()

            print result

            with open("stats_{0}.csv".format(fntx), "w") as text_file:
                fntx += 1
                text_file.write(result)

        conn.close()
    except psycopg2.Error as e:
        print "database_stats(): Eroare baza de date: ", e[0]
    except Exception as e:
        print "database_stats(): Eroare: ", e[0]


def db_test(test_type):
    codes_list = ["1003", "A00", "W1110227", "NU_EXISTA", "1", "MH64X", "2", "BA0", "LSG337350", "TU16"]
    queries_list = []
    query_id = 0
    
    for code in codes_list:
        if code == "":
            continue
        queries_list.append((query_id, code))
        query_id += 1

    total_queries = len(queries_list)
    
    if total_queries > 0:
        pool = multiprocessing.Pool(total_queries)
        if test_type == "search":
            pool.map(sql_worker_search, queries_list, 1)
        elif test_type == "search_with_gest":
            pool.map(sql_worker_search_with_gest, queries_list, 1)
        pool.close()
        pool.join()


def sql_worker_search(query_tuple):
    try:
        print "exec query {0}, cod '{1}'...".format(query_tuple[0], query_tuple[1])
        start_time = timeit.default_timer()
        
        conn_cat = psycopg2.connect(host=g_host, database="autoland_catalog", user=g_user, password=g_passw)

        sql_search = (
                    """SELECT tbl_suppliers.denumire,
                        tbl_components.supp_id, tbl_components.code, tbl_components.new_code, tbl_components.description, tbl_components.price, tbl_components.warranty,
                        tbl_discounts.val_disc 
                        FROM tbl_suppliers
                        JOIN tbl_components ON (tbl_components.supp_id = tbl_suppliers.supp_id) 
                        JOIN tbl_discounts ON (tbl_discounts.supp_id = tbl_components.supp_id AND tbl_components.gr_disc = tbl_discounts.gr_disc) 
                        WHERE tbl_components.code LIKE '{0}%' ORDER BY tbl_components.supp_id DESC, tbl_components.code LIMIT 5""".format(query_tuple[1])
                        )

        cur_cat = conn_cat.cursor()
        cur_cat.execute(sql_search)

        cur_cat.fetchall()
        
        elapsed = timeit.default_timer() - start_time
        print "query {0}, cod '{1}', {2} articole, finalizat in {3} sec".format(query_tuple[0], query_tuple[1], cur_cat.rowcount, round(elapsed, 3))
        
        cur_cat.close()
        conn_cat.close()
    except psycopg2.Error as e:
        print "sql_worker_search(): Eroare baza de date: ", e[0]
    except Exception as e:
        print "sql_worker_search(): Eroare: ", e[0]


def sql_worker_search_with_gest(query_tuple):
    try:
        print "exec query {0}, cod '{1}'...".format(query_tuple[0], query_tuple[1])
        start_time = timeit.default_timer()

        conn_cat = psycopg2.connect(host=g_host, database="autoland_catalog", user=g_user, password=g_passw)
        conn_cont = psycopg2.connect(host=g_host, database="autoland", user=g_user, password=g_passw)

        sql_search = (
            """SELECT tbl_suppliers.denumire,
                tbl_components.supp_id, tbl_components.code, tbl_components.new_code, tbl_components.description, tbl_components.price, tbl_components.warranty,
                tbl_discounts.val_disc
                FROM tbl_suppliers
                JOIN tbl_components ON (tbl_components.supp_id = tbl_suppliers.supp_id)
                JOIN tbl_discounts ON (tbl_discounts.supp_id = tbl_components.supp_id AND tbl_components.gr_disc = tbl_discounts.gr_disc)
                WHERE tbl_components.code LIKE '{0}%' ORDER BY tbl_components.supp_id DESC, tbl_components.code LIMIT 5""".format(query_tuple[1])
        )

        cur_cat = conn_cat.cursor()
        cur_cat.execute(sql_search)

# interogheaza gestiunile pentru fiecare cod returnat
        cat_rows = cur_cat.fetchall()
        for row in cat_rows:
            sql_gest = (
                """SELECT M.cod_gest, N.UM, S.StocInit + CASE WHEN A.Intrari IS NULL THEN 0 ELSE A.Intrari END - CASE WHEN B.Iesiri IS NULL THEN 0 ELSE B.Iesiri END AS Stoc
                FROM (SELECT SUBSTR(CodArtLot,1,16) AS CodArt, cod_gest, SUM(Stoc_Init) AS StocInit FROM STOCMARF GROUP BY CodArt, cod_gest) S
                LEFT JOIN (SELECT SUBSTR(CodArtLot,1,16) AS CodArt, cod_gest, SUM(Cantitate) AS Intrari FROM MISCMARF WHERE Cod_Oper<'50' GROUP BY CodArt, cod_gest) A
                ON (RTRIM(S.CodArt) = RTRIM(A.CodArt) AND S.cod_gest = A.cod_gest)
                LEFT JOIN (SELECT SUBSTR(CodArtLot,1,16) AS CodArt, cod_gest, SUM(Cantitate) AS Iesiri FROM MISCMARF WHERE Cod_Oper>='50' GROUP BY CodArt, cod_gest) B
                ON (RTRIM(S.CodArt) = RTRIM(B.CodArt) AND S.cod_gest = B.cod_gest)
                LEFT JOIN (SELECT Cod_Art, RTRIM(Den_Art)::char(50) AS DenArt, UM FROM CATMARF) N
                ON RTRIM(S.CodArt) = RTRIM(N.Cod_Art)
                LEFT JOIN (SELECT cod_gest, den_gest FROM GESTMARF) M
                ON S.cod_gest = M.cod_gest
                WHERE S.StocInit + CASE WHEN A.Intrari IS NULL THEN 0 ELSE A.Intrari END - CASE WHEN B.Iesiri IS NULL THEN 0 ELSE B.Iesiri END <> 0
                AND RTRIM(S.CodArt) = '{0}'""".format(row[2])
            )
            cur_cont = conn_cont.cursor()
            cur_cont.execute(sql_gest)
            cur_cont.fetchall()
            cur_cont.close()

        elapsed = timeit.default_timer() - start_time
        print "query {0}, cod '{1}', {2} articole, finalizat in {3} sec".format(query_tuple[0], query_tuple[1], cur_cat.rowcount, round(elapsed, 3))

        cur_cat.close()

        conn_cont.close()
        conn_cat.close()
    except psycopg2.Error as e:
        print "sql_worker_search_with_gest(): Eroare baza de date: ", e[0]
    except Exception as e:
        print "sql_worker_search_with_gest(): Eroare: ", e[0]


if __name__ == '__main__':
    print "Copyright (c) 2016, Mihai Greavu"
    print "Stress-test pentru baza de date autoland_catalog"
    print "Parametrii posibili:"
    print "\tstats = statistici baza de date"
    print "\tsearch = cauta multiple coduri in paralel"
    print "\tsearch_with_gest = cauta si coduri si disponibilitatea lor in gestiuni in paralel"
    print

    if len(sys.argv) != 2:
        print "Numar invalid de parametrii"    
    else:
        if sys.argv[1] == "stats":
            db_stats()
        elif sys.argv[1] == "search":
            db_test("search")
        elif sys.argv[1] == "search_with_gest":
            db_test("search_with_gest")
        else:
            print "Parametru invalid: '{0}'".format(sys.argv[1])
