#!/usr/bin/env python

# INPUT:
# t722759680			2016-10-07 13:52:24	NG_ReportNewPosition 23.5944 46.7552 105.914 1.97222 1 1.3 436.7

import sys
import re
import time


# 46.753014 23.584244, 214.936523, 4.813889, 10.000000, 1461049218797807, 19.04.2016 09:00:18
def gen_raw_csv(gpsdata, epoch):
    epoch *= 1000000
    line = gpsdata[7] + " " + gpsdata[6] + ", " + gpsdata[8] + ", " + gpsdata[9] + ", " + "10.000000" + ", " \
        + str(epoch) + ", " + time.strftime("%d.%m.%Y") + " " + gpsdata[3] + ":" + gpsdata[4] + ":" + gpsdata[5]\
        + "\n"
    return line


# 46.7544167798787,23.5932683944702,269.225779835072,-1,-1
def gen_matched_csv(gpsdata):
    line = gpsdata[7] + "," + gpsdata[6] + "," + gpsdata[8] + ",-1,-1" + "\n"
    return line


if __name__ == '__main__':
    print("\nExtract GPS positions from the Skobbler log")
    print("Parameters:")
    print("\t--raw | --matched")

    if len(sys.argv) == 2:
        mode = 9999
        outGpsFileName = "GPSPositions"
        secondsSinceEpoch = int(time.time())

        if sys.argv[1] == "--matched":
            mode = 1
            outGpsFileName += "Matched"
        elif sys.argv[1] == "--raw":
            mode = 0
            outGpsFileName += "Raw"

        if mode == 0 or mode == 1:
            with open('NG_API.appLog', 'r') as fIn:
                with open(outGpsFileName + '.csv', 'w') as fOut:
                    for inLine in fIn:
                        if "NG_ReportNewPosition" not in inLine:
                            continue

                        arrGpsData = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", inLine)

                        if len(arrGpsData) != 13:
                            print("INVALID LINE: '{0}'".format(inLine))
                            continue

                        if mode == 0:
                            outLine = gen_raw_csv(arrGpsData, secondsSinceEpoch)
                            secondsSinceEpoch += 1
                        elif mode == 1:
                            outLine = gen_matched_csv(arrGpsData)

                        fOut.write(outLine)
                    print("Done")
        else:
            print('Invalid parameter: {0}'.format(mode))
    else:
        print("Invalid number of parameters")
