import os
import sqlite3


def create_catalog(dest_path):
    db_filename = os.path.join(dest_path, "Vcat.sqlite")
    db_is_new = not os.path.exists(db_filename)
    conn = sqlite3.connect(db_filename)

    if db_is_new:
        query_tbl_video = """CREATE TABLE video (
                            vid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                            path TEXT UNIQUE NOT NULL
                            )"""

        query_tbl_video_frames = """CREATE TABLE video_frames (
                                vid INTEGER,
                                frameid INTEGER,
                                image BLOB,
                                FOREIGN KEY(vid) REFERENCES video(vid))"""

        conn.execute(query_tbl_video)
        conn.execute(query_tbl_video_frames)
    return conn


def close_catalog(conn):
    conn.close()


def insert_video(conn, video_path):
    query_insert = """INSERT INTO video(path) VALUES('{0}')""".format(video_path)
    cursor = conn.cursor()
    cursor.execute(query_insert)
    lastrowid = cursor.lastrowid
    conn.commit()
    cursor.close()
    return lastrowid


def insert_frame(conn, vid, images):
    if len(images) > 0:
        cursor = conn.cursor()
        frameid = 0
        for image in images:
            query_insert = """INSERT INTO video_frames(vid, frameid, image) VALUES({0}, {1}, '{2}')""".format(vid, frameid, image)
            print query_insert
            cursor.execute(query_insert)
            frameid += 1

        conn.commit()
        cursor.close()
