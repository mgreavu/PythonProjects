import sys
import os
import re
import VideoCatalog as Vcat


def list_videos(root_path):
    if not os.path.isdir(root_path):
        print "\nDirectory '{0}' not found".format(root_path)
    else:
        fcount = 0
        fsize = 0
        for curr_path, subdirs, files in os.walk(root_path):
            print "\n{0}".format(curr_path)
            for name in files:
                statinfo = os.stat(os.path.join(curr_path, name))
                print "\t{0}\t{1} MB".format(name, statinfo.st_size / 1048576)
                fcount += 1
                fsize += statinfo.st_size
        print "\n{0} files in '{1}' for a total of {2} MB".format(fcount, root_path, fsize / 1048576)


def search_files(root_path, search_str):
    found_list = []
    if not os.path.isdir(root_path):
        print "\nDirectory '{0}' not found".format(root_path)
    elif len(search_str) == 0:
        print "\nEmpty search expression"
    else:
        for curr_path, subdirs, files in os.walk(root_path):
            for name in files:
                if re.search(search_str, name, re.IGNORECASE):
                    print "\t{0}".format(os.path.join(curr_path, name))
                    found_list.append(name)
        print "\nFound {0} matches in '{1}'".format(len(found_list), root_path)
    return found_list


def cache_videos(conn, source_path, dest_path):
    if not os.path.isdir(source_path):
        print "\nSource directory '{0}' not found".format(source_path)
    elif not os.path.isdir(dest_path):
        print "\nDestination directory '{0}' not found".format(dest_path)
    else:
        for curr_path, subdirs, files in os.walk(source_path):
            for name in files:
                src_file = os.path.join(curr_path, name)
                name_no_ext = os.path.splitext(name)[0]
                dst_file = os.path.join(dest_path, name_no_ext)
                os.system("ffmpeg -i \"{0}\" -vf fps=1/60,scale=320:-1 \"{1}_%03d.jpg\"".format(src_file, dst_file))
                # insert into database
                images_list = search_files(dest_path, name_no_ext)
                vid = Vcat.insert_video(conn, src_file)
                Vcat.insert_frame(conn, vid, images_list)


if __name__ == '__main__':
    print "Video Files Manager. Copyright (c) 2017, Mihai Greavu\n"

    paramcount = len(sys.argv)
    if paramcount == 1:
        print "Possible parameters:"
        print "\t--list = list video files"
        print "\t--search = search for videos"
        print "\t--cache = cache videos"
    else:
        if sys.argv[1] == "--list":
            if paramcount == 3:
                list_videos(sys.argv[2])
            else:
                print "Invalid --list command parameters, need target dir"
        elif sys.argv[1] == "--search":
            if paramcount == 4:
                search_files(sys.argv[2], sys.argv[3])
            else:
                print "Invalid --search command parameters, need target dir and search string"
        elif sys.argv[1] == "--cache":
            if paramcount == 4:
                sql_conn = Vcat.create_catalog(sys.argv[3])
                cache_videos(sql_conn, sys.argv[2], sys.argv[3])
                Vcat.close_catalog(sql_conn)
            else:
                print "Invalid --cache command parameters, need source and dest dirs"
        else:
            print "Invalid parameter: '{0}'".format(sys.argv[1])
