import sys
import os
from shutil import copyfile


def list_files(root_path):
    if not os.path.isdir(root_path):
        print(f'Directory {root_path} not found')
    else:
        fcount = 0
        fsize = 0
        for curr_path, subdirs, files in os.walk(root_path):
            print(f'\n{curr_path}')
            for name in files:
                statinfo = os.stat(os.path.join(curr_path, name))
                print('\t{0}\t{1:.2f} MB'.format(name, statinfo.st_size / 1048576))
                fcount += 1
                fsize += statinfo.st_size
        print('\n{0} files in {1} for a total of {2:.2f} MB'.format(fcount, root_path, fsize / 1048576))


def copy_file(source_file_path, dest_folder, how_many):
    if not os.path.isdir(dest_folder):
        print(f'Destination folder {dest_folder} not found')
    elif not os.path.isfile(source_file_path):
        print('\nSource file does not exist')
    elif how_many <= 0:
        print('\nThe number of copies must be positive')
    else:
        print(f'Copying {source_file_path}...')

        source_path, source_file = os.path.split(source_file_path)
        source_root, source_ext = os.path.splitext(source_file)
        for k in range(0, how_many):
            dest_file = dest_folder + "\\"
            dest_file += source_root
            dest_file += "_"
            dest_file += str(k)
            dest_file += source_ext

            copyfile(source_file_path, dest_file)
        print('Done')


if __name__ == '__main__':
    print('File Copier. Copyright (c) 2018, Mihai Greavu\n')

    paramcount = len(sys.argv)
    if paramcount == 1:
        print('Possible parameters:')
        print('\t--list = list files')
        print('\t--copy = copy file to destination')
    else:
        if sys.argv[1] == "--list":
            if paramcount == 3:
                list_files(sys.argv[2])
            else:
                print('Invalid --list command parameters, need target dir')
        elif sys.argv[1] == "--copy":
            if paramcount == 5:
                copy_file(sys.argv[2], sys.argv[3], int(sys.argv[4]))
            else:
                print('Invalid --copy command parameters, need source file name, destination folder and copy count')
        else:
            print(f'Invalid parameter: {sys.argv[1]}')
