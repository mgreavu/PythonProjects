## PythonProjects

Diverse scripturi folositoare

### AutolandCatalog
Teste cu baza de date autoland_catalog

### VideoManager
Extrage informatii si frame-uri din fisierele video din folderul sursa.
Le stocheaza intr-o baza de date SQLite. Foloseste ffmpeg.

### FileCopier
Produce 5000 de copii ale fisierului sursa in fisierul destinatie.

### SkobblerLogGPSExtractor
Extrage informatii despre pozitiile GPS din fisierul log al librariei Skobbler si le salveaza intr-un fisier cu format CSV.

### BUIGPSParser
Parse, extract, analyze and plot raw and matched GPS positions on BUI350 and BUI270
