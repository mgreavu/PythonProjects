#!/usr/bin/env python

import Globals
import BUI350Parser as Bui350
import BUI270Parser as Bui270
import GPSPlot
import sys
import os
import datetime

from math import sin, cos, sqrt, atan2, radians


def geo_distance(lat1, lon1, lat2, lon2):
    # approximate radius of Earth in m
    re = 6373000

    lat1_r = radians(lat1)
    lon1_r = radians(lon1)
    lat2_r = radians(lat2)
    lon2_r = radians(lon2)

    dlon = lon2_r - lon1_r
    dlat = lat2_r - lat1_r

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    dist = re * c
    return dist


def analyze_matched_positions(root_path, delay_threshold, speed_threshold):
    if not os.path.isdir(root_path):
        print("\nFolder '{0}' not found".format(root_path))
    else:
        for curr_path, subdirs, files in os.walk(root_path):
            for subdir in subdirs:
                datapath = curr_path + subdir + "/"

                pos_between_matches = 0
                speed_between_matches = 0
                accuracy_between_matches = 0

                # from bui350
                gpsdatafname = datapath + Globals.BUI350_GPS_FILENAME
                if os.path.isfile(gpsdatafname):
                    with open(gpsdatafname, 'r') as fIn:
                        report = datapath + "MapMatchingDelay_" + subdir + "_" + Globals.TAG_DEVICE_BUI350 + ".csv"
                        with open(report, 'w') as fRpt:
                            fRpt.write('last_match_time;last_match_lat;last_match_lon;'
                                       'curr_match_time;curr_match_lat;curr_match_lon;'
                                       'elapsed_time;distance_delta;positions_received_in-between;'
                                       'avg_accuracy;avg_speed\n')
                            last_matched = (Globals.TAG_DEVICE_BUI350, Globals.TAG_MATCHED, 0, 0, '')
                            for line in fIn:
                                if Globals.TAG_RECEIVED in line:
                                    tokens = line.split(Globals.CSV_SEPARATOR)

                                    accuracy = float(tokens[6])
                                    speed = float(tokens[8])

                                    # verify LocationManager timestamp against ours
                                    dt_locman = datetime.datetime.strptime(tokens[9][:-1], "%Y-%m-%d %H:%M:%S")
                                    dt_ours = datetime.datetime.strptime(tokens[3], "%Y-%m-%d %H:%M:%S")
                                    delta = dt_ours - dt_locman
                                    secs = delta.total_seconds()
                                    if secs > 1.0:
                                        print('*** {0}: {1} seconds between LocationManager timestamp and ours ***'
                                              .format(subdir, secs))

                                    pos_between_matches += 1
                                    accuracy_between_matches += accuracy
                                    speed_between_matches += speed

                                elif Globals.TAG_MATCHED in line:
                                    tokens = line.split(Globals.CSV_SEPARATOR)

                                    device_id = tokens[0]
                                    position_type = tokens[2]
                                    dt = tokens[3]
                                    latitude = float(tokens[4])
                                    longitude = float(tokens[5])

                                    if len(last_matched[4]) > 0:
                                        dt_previous = datetime.datetime.strptime(last_matched[4],
                                                                                 "%Y-%m-%d %H:%M:%S")
                                        dt_current = datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
                                        delta = dt_current - dt_previous
                                        secs = delta.total_seconds()
                                        dist = geo_distance(
                                            latitude, longitude, last_matched[2], last_matched[3])
                                        if secs >= delay_threshold:
                                            if pos_between_matches == 0:
                                                fRpt.write('{0};{1};{2};{3};{4};{5};{6:.2f};{7:.2f};0;;\n'.format(
                                                        last_matched[4][11:], last_matched[2], last_matched[3],
                                                        dt[11:], latitude, longitude,
                                                        secs, dist))
                                            else:
                                                avg_speed = speed_between_matches / pos_between_matches
                                                if avg_speed >= speed_threshold:
                                                    fRpt.write('{0};{1};{2};{3};{4};{5};{6:.2f};{7:.2f};{8};{9:.2f};'
                                                               '{10:.2f}\n'
                                                               .format(
                                                                last_matched[4][11:], last_matched[2], last_matched[3],
                                                                dt[11:], latitude, longitude,
                                                                secs, dist, pos_between_matches,
                                                                accuracy_between_matches / pos_between_matches,
                                                                avg_speed))

                                    last_matched = (device_id, position_type, latitude, longitude, dt)
                                    pos_between_matches = 0
                                    accuracy_between_matches = 0
                                    speed_between_matches = 0
                else:
                    print('{0}: BUI350 GPS data file not found, skipped from analysis'.format(subdir))

                pos_between_matches = 0
                speed_between_matches = 0
                accuracy_between_matches = 0

                # from bui270
                gpsdatafname = datapath + Globals.BUI270_GPS_FILENAME
                if os.path.isfile(gpsdatafname):
                    with open(gpsdatafname, 'r') as fIn:
                        report = datapath + "MapMatchingDelay_" + subdir + "_" + Globals.TAG_DEVICE_BUI270 + ".csv"
                        with open(report, 'w') as fRpt:
                            fRpt.write('last_match_time;last_match_lat;last_match_lon;'
                                       'curr_match_time;curr_match_lat;curr_match_lon;'
                                       'elapsed_time;distance_delta;positions_received_in-between;'
                                       'avg_accuracy;avg_speed\n')
                            last_matched = (Globals.TAG_DEVICE_BUI270, Globals.TAG_MATCHED, 0, 0, '')
                            for line in fIn:
                                if Globals.TAG_PROCESSED in line:
                                    tokens = line.split(Globals.CSV_SEPARATOR)

                                    accuracy = float(tokens[6])
                                    if "N/A" in tokens[8]:
                                        speed = 0
                                    else:
                                        speed = float(tokens[8])

                                    pos_between_matches += 1
                                    accuracy_between_matches += accuracy
                                    speed_between_matches += speed

                                elif Globals.TAG_MATCHED in line:
                                    tokens = line.split(Globals.CSV_SEPARATOR)

                                    device_id = tokens[0]
                                    position_type = tokens[2]
                                    dt = tokens[3][:-4]
                                    latitude = float(tokens[4])
                                    longitude = float(tokens[5])

                                    if len(last_matched[4]) > 0:
                                        dt_previous = datetime.datetime.strptime(last_matched[4], "%H:%M:%S")
                                        dt_current = datetime.datetime.strptime(dt, "%H:%M:%S")
                                        delta = dt_current - dt_previous
                                        secs = delta.total_seconds()
                                        dist = geo_distance(
                                            latitude, longitude, last_matched[2], last_matched[3])
                                        if secs >= delay_threshold:
                                            if pos_between_matches == 0:
                                                fRpt.write('{0};{1};{2};{3};{4};{5};{6:.2f};{7:.2f};0;;\n'.format(
                                                        last_matched[4], last_matched[2], last_matched[3],
                                                        dt, latitude, longitude,
                                                        secs, dist))
                                            else:
                                                avg_speed = speed_between_matches / pos_between_matches
                                                if avg_speed >= speed_threshold:
                                                    fRpt.write('{0};{1};{2};{3};{4};{5};{6:.2f};{7:.2f};{8};{9:.2f};'
                                                               '{10:.2f}\n'
                                                               .format(
                                                                last_matched[4], last_matched[2], last_matched[3],
                                                                dt, latitude, longitude,
                                                                secs, dist, pos_between_matches,
                                                                accuracy_between_matches / pos_between_matches,
                                                                avg_speed))

                                    last_matched = (device_id, position_type, latitude, longitude, dt)
                                    pos_between_matches = 0
                                    accuracy_between_matches = 0
                                    speed_between_matches = 0
                else:
                    print('{0}: BUI270 GPS data file not found, skipped from analysis'.format(subdir))


# parse BUI270's S28-shutdownd log, has matched positions and 4 decimals only for lat/lon
def parse_test_data(root_path):
    if not os.path.isdir(root_path):
        print("\nFolder '{0}' not found".format(root_path))
    else:
        for curr_path, subdirs, files in os.walk(root_path):
            for name in files:
                if name == Globals.BUI270_LOG_FILENAME:
                    log_name = curr_path
                    log_name += "/"
                    log_name += name
                    print("Processing '{0}'...".format(log_name))
                    Bui270.parse(log_name)
                elif name == Globals.BUI350_LOG_FILENAME:
                    log_name = curr_path
                    log_name += "/"
                    log_name += name
                    print("Processing '{0}'...".format(log_name))
                    Bui350.parse(log_name)


# parse BUI270's RawData log, no matched positions in it
def parse_test_data_raw(root_path):
    if not os.path.isdir(root_path):
        print("\nFolder '{0}' not found".format(root_path))
    else:
        for curr_path, subdirs, files in os.walk(root_path):
            for name in files:
                if name == Globals.BUI270_RAW_DATA_FILENAME:
                    log_name = curr_path
                    log_name += "/"
                    log_name += name
                    print("Processing '{0}'...".format(log_name))
                    Bui270.parse_raw_data(log_name)
                elif name == Globals.BUI350_LOG_FILENAME:
                    log_name = curr_path
                    log_name += "/"
                    log_name += name
                    print("Processing '{0}'...".format(log_name))
                    Bui350.parse(log_name)


if __name__ == '__main__':
    print("\n")
    print("Extract and analyze GPS data from Navigation logs.")
    print("Place the input files in the ./data subfolder.")
    print("Parameters:")
    print("\t--parse [normal | raw]")
    print("\t--analyze")
    print("\t--plot <testname>")
    print("\n")

    if len(sys.argv) == 2:
        if sys.argv[1] == "--analyze":
            analyze_matched_positions("data/", 3.0, 1.0)
            print("completed.")
        else:
            print("Invalid parameter")
    elif len(sys.argv) == 3:
        if sys.argv[1] == "--parse":
            if sys.argv[2] == "normal":
                parse_test_data("data/")
            elif sys.argv[2] == "raw":
                parse_test_data_raw("data/")
        elif sys.argv[1] == "--plot":
            testpath = "data/"
            testpath += sys.argv[2]
            testpath += "/"
            GPSPlot.plot_test_data(testpath, True)
        else:
            print("Invalid parameters")
    else:
        print("Invalid parameters")
