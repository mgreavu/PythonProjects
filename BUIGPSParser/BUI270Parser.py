import Globals
import re


def get_time(line):
    match = re.search(r'\d{2}:\d{2}:\d{2}.\d{3}', line)
    if match is not None:
        return match.group(0)
    return ""


# [1488286948.027053] 13:02:28.026 skobbler debug(1085:0)/skobbler skobblerservice.cpp(1018)
# setPosition: latitude: 46.7606 longitude: 23.5927 speed: 2.55833 heading: 172.595
# horizontalAccuracy: 0.8 verticalAccuracy: 1.2 altitude: 414.2
def handle_processed(gpsdata):
    # device
    line = Globals.TAG_DEVICE_BUI270
    line += Globals.CSV_SEPARATOR
    # position id
    # N/A
    line += Globals.CSV_SEPARATOR
    # status
    line += Globals.TAG_PROCESSED
    line += Globals.CSV_SEPARATOR
    # datetime
    line += get_time(gpsdata)
    line += Globals.CSV_SEPARATOR

    content = gpsdata.split("setPosition: ", 1)[1]
    tokens = content.split(" ")

    if len(tokens) >= 14:
        # latitude
        line += tokens[1]
        line += Globals.CSV_SEPARATOR
        # longitude
        line += tokens[3]
        line += Globals.CSV_SEPARATOR
        # accuracy (horizontal)
        line += tokens[9]
        line += Globals.CSV_SEPARATOR
        # heading
        line += tokens[7]
        line += Globals.CSV_SEPARATOR
        # speed
        line += tokens[5]
    else:
        line += Globals.CSV_SEPARATOR
        line += Globals.CSV_SEPARATOR
        line += Globals.CSV_SEPARATOR
        line += Globals.CSV_SEPARATOR

    line += "\n"
    return line


# [1488286952.834972] 13:02:32.834 Main debug(772:0)/skobbler skobblerproxy.cpp(831)
# setMatchedPosition: Matched position received: QGeoCoordinate(46.7606, 23.5927)
def handle_matched(gpsdata):
    # device
    line = Globals.TAG_DEVICE_BUI270
    line += Globals.CSV_SEPARATOR
    # position id
    # N/A
    line += Globals.CSV_SEPARATOR
    # status
    line += Globals.TAG_MATCHED
    line += Globals.CSV_SEPARATOR
    # time
    line += get_time(gpsdata)
    line += Globals.CSV_SEPARATOR

    index = gpsdata.rfind("QGeoCoordinate", 0)
    if index != -1:
        coord = gpsdata[index + len("QGeoCoordinate") + 1:-2]
        tokens = coord.split(",")
        # latitude
        line += tokens[0]
        line += Globals.CSV_SEPARATOR
        # longitude
        line += tokens[1][1:]
    else:
        line += Globals.CSV_SEPARATOR

    line += "\n"
    return line


def parse(filename):
    with open(filename, 'r') as fIn:
        csvfilename = filename.replace(Globals.BUI270_LOG_FILENAME, Globals.BUI270_GPS_FILENAME)
        with open(csvfilename, 'w') as fOut:
            for inLine in fIn:
                if "setPosition: latitude: " in inLine:
                    out = handle_processed(inLine)
                    fOut.write(out)
                elif "reportMatchedPosition" in inLine:
                    out = handle_matched(inLine)
                    fOut.write(out)
                else:
                    continue


def weeksecondstoutc(gpsweek, gpsseconds, leapseconds):
    import datetime
    datetimeformat = "%Y-%m-%d %H:%M:%S"
    epoch = datetime.datetime.strptime("1980-01-06 00:00:00", datetimeformat)
    elapsed = datetime.timedelta(days=(gpsweek * 7), seconds=(gpsseconds + leapseconds))
    return datetime.datetime.strftime(epoch + elapsed, datetimeformat)


def parse_raw_data(filename):
    with open(filename, 'r') as fIn:
        csvfilename = filename.replace(Globals.BUI270_RAW_DATA_FILENAME, Globals.BUI270_GPS_FILENAME)
        with open(csvfilename, 'w') as fOut:
            for line in fIn:
                tokens = line.split(",")
                if len(tokens) >= 15:
                    # device
                    out = Globals.TAG_DEVICE_BUI270
                    out += Globals.CSV_SEPARATOR
                    # position id
                    out += "N/A"
                    out += Globals.CSV_SEPARATOR
                    # status
                    out += Globals.TAG_PROCESSED
                    out += Globals.CSV_SEPARATOR
                    # datetime
                    # GPS time was zero at 0h 6-Jan-1980 and since it is not perturbed by leap seconds
                    # GPS is now ahead of UTC by 18 seconds
                    out += weeksecondstoutc(int(tokens[14]), int(tokens[15])/1000, -18)
                    out += Globals.CSV_SEPARATOR
                    # latitude
                    out += tokens[0]
                    out += Globals.CSV_SEPARATOR
                    # longitude
                    out += tokens[1]
                    out += Globals.CSV_SEPARATOR
                    # accuracy (uncert_north)
                    out += tokens[2]
                    out += Globals.CSV_SEPARATOR
                    # heading
                    out += tokens[10]
                    out += Globals.CSV_SEPARATOR
                    # speed
                    out += "N/A"

                    out += "\n"

                    fOut.write(out)
                else:
                    print("FAILED: {0}".format(line))
