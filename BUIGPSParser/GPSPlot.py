# -*- coding: utf-8 -*-

import Globals
import os.path
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt


def prepare_gps_data(subfolder):
    # tuple(DEVICE_ID, POSITION_TYPE, datetime, latitude, longitude, accuracy, heading, speed)
    gps_positions = []

    # from bui350
    gpsdatafname = subfolder + Globals.BUI350_GPS_FILENAME
    if os.path.isfile(gpsdatafname):
        with open(gpsdatafname, 'r') as fIn:
            for line in fIn:
                if Globals.TAG_MATCHED in line:
                    tokens = line.split(Globals.CSV_SEPARATOR)
                    gps_positions.append(tuple((tokens[0], tokens[2], tokens[3], float(tokens[4]), float(tokens[5]),
                                                -1, -1, -1)))
                elif Globals.TAG_RECEIVED in line:
                    tokens = line.split(Globals.CSV_SEPARATOR)
                    gps_positions.append(tuple((tokens[0], tokens[2], tokens[3], float(tokens[4]), float(tokens[5]),
                                                float(tokens[6]), round(float(tokens[7])), float(tokens[8]))))
    else:
        print("BUI350 GPS data file not found, skipped from plotting")

    # from bui270
    gpsdatafname = subfolder + Globals.BUI270_GPS_FILENAME
    if os.path.isfile(gpsdatafname):
        with open(gpsdatafname, 'r') as fIn:
            for line in fIn:
                if Globals.TAG_MATCHED in line:
                    tokens = line.split(Globals.CSV_SEPARATOR)
                    gps_positions.append(tuple((tokens[0], tokens[2], tokens[3], float(tokens[4]), float(tokens[5]),
                                                -1, -1, -1)))
                elif Globals.TAG_PROCESSED in line:
                    tokens = line.split(Globals.CSV_SEPARATOR)
                    gps_positions.append(tuple((tokens[0], tokens[2], tokens[3], float(tokens[4]), float(tokens[5]),
                                                float(tokens[6]), round(float(tokens[7])), -1)))
    else:
        print("BUI270 GPS data file not found, skipped from plotting")

    return gps_positions


def bounding_box(positions):
    # bounding box(x_min, y_min, x_max, y_max)
    x_min = 180.0
    y_min = 90.0
    x_max = -180.0
    y_max = -90.0
    for position in positions:
        if position[4] < x_min:
            x_min = position[4]
        if position[4] > x_max:
            x_max = position[4]
        if position[3] < y_min:
            y_min = position[3]
        if position[3] > y_max:
            y_max = position[3]

    # 1 degree = 111km at Equator, 55km at 45 degrees latitude
    expand = 0.001
    x_min = x_min - expand
    y_min = y_min - expand
    x_max = x_max + expand
    y_max = y_max + expand

    return x_min, y_min, x_max, y_max


def plot_test_data(subfolder, annotations):
    gps_positions = prepare_gps_data(subfolder)
    x_min, y_min, x_max, y_max = bounding_box(gps_positions)
    print("BBOX: SW = ({0:.5f}, {1:.5f}), NE = ({2:.5f}, {3:.5f})".format(x_min, y_min, x_max, y_max))

    fig, ax = plt.subplots(figsize=(12.8, 8))

    m = Basemap(
        resolution='c',
        projection='merc',
        llcrnrlon=x_min, llcrnrlat=y_min, urcrnrlon=x_max, urcrnrlat=y_max)

    if "cluj" in subfolder:
        m.readshapefile('maps/cluj', 'roads', drawbounds=True, linewidth=0.5, color='grey')
    elif "lund" in subfolder:
        m.readshapefile('maps/lund', 'roads', drawbounds=True, linewidth=0.5, color='grey')
    elif "coimbatore" in subfolder:
        m.readshapefile('maps/coimbatore', 'roads', drawbounds=True, linewidth=0.5, color='grey')
    elif "renningen" in subfolder:
        m.readshapefile('maps/renningen', 'roads', drawbounds=True, linewidth=0.5, color='grey')
    else:
        print("No map data found for this area")

    longs_bui350_received = []
    lats_bui350_received = []
    longs_bui350_matched = []
    lats_bui350_matched = []

    longs_bui270_processed = []
    lats_bui270_processed = []
    longs_bui270_matched = []
    lats_bui270_matched = []

    k350 = 0
    k270 = 0
    for gps_position in gps_positions:
        if gps_position[0] == Globals.TAG_DEVICE_BUI350:
            if gps_position[1] == Globals.TAG_RECEIVED:
                xpt, ypt = m(gps_position[4], gps_position[3])
                longs_bui350_received.append(xpt)
                lats_bui350_received.append(ypt)
            elif gps_position[1] == Globals.TAG_MATCHED:
                xpt, ypt = m(gps_position[4], gps_position[3])
                longs_bui350_matched.append(xpt)
                lats_bui350_matched.append(ypt)
        elif gps_position[0] == Globals.TAG_DEVICE_BUI270:
            if gps_position[1] == Globals.TAG_PROCESSED:
                xpt, ypt = m(gps_position[4], gps_position[3])
                longs_bui270_processed.append(xpt)
                lats_bui270_processed.append(ypt)
            elif gps_position[1] == Globals.TAG_MATCHED:
                xpt, ypt = m(gps_position[4], gps_position[3])
                longs_bui270_matched.append(xpt)
                lats_bui270_matched.append(ypt)

        if annotations is True:
            if gps_position[0] == Globals.TAG_DEVICE_BUI350:
                if gps_position[1] == Globals.TAG_RECEIVED:
                    if k350 % 15 == 0:
                        dt = gps_position[2][11:]
                        ann = '{0}, {1:.1f} m, {2}°, {3:.2f} m/s'.format(dt, gps_position[5], gps_position[6],
                                                                         gps_position[7])
                        ax.annotate(ann, xy=(xpt, ypt), xycoords='data', xytext=(xpt, ypt), textcoords='data',
                                    color='red')
                    k350 += 1
            elif gps_position[0] == Globals.TAG_DEVICE_BUI270:
                if gps_position[1] == Globals.TAG_PROCESSED:
                    if k270 % 5 == 0:
                        dt = gps_position[2]
                        if len(dt) > 12:
                            dt = dt[11:]
                        ann = '{0}, {1:.1f} m, {2}°'.format(dt, gps_position[5], gps_position[6])
                        ax.annotate(ann, xy=(xpt, ypt), xycoords='data', xytext=(xpt, ypt), textcoords='data',
                                    color='green')
                    k270 += 1

    m.plot(longs_bui350_received, lats_bui350_received, color='red', marker='o', markersize=4)
    m.plot(longs_bui350_matched, lats_bui350_matched, color='black', marker='^', markersize=4)
    m.plot(longs_bui270_processed, lats_bui270_processed, color='green', marker='o', markersize=4)
    m.plot(longs_bui270_matched, lats_bui270_matched, color='blue', marker='^', markersize=4)

    plt.title('GPS positions in ' + subfolder)
    fig.tight_layout()
    plt.show()

    # plt.savefig('zorilor.png')
