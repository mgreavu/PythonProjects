import Globals
import re
import datetime


def get_position_id(line):
    match = re.search(r'GPS position (\d+)', line)
    if match is not None:
        return match.group(1)
    return ""


# 729 2018/11/21 13:55:27.806896 49.6568 5 3xx Navi /GPS 492 log warn verbose 1 /GPSClientPrivate.cpp@200
# [492:1995445328] onPositionChangedSignal() GPS position 1 [RECEIVED]
# @ Thu Sep 17 10:03:01 2015 , (48.475607, 9.064786), accu = 4.30, head = 174.69, speed = 27.78, tstamp = 1442484181312
def handle_received(gpsdata):
    # device
    line = Globals.TAG_DEVICE_BUI350
    line += Globals.CSV_SEPARATOR
    # position id
    line += get_position_id(gpsdata)
    line += Globals.CSV_SEPARATOR
    # status
    line += Globals.TAG_RECEIVED
    line += Globals.CSV_SEPARATOR

    content = gpsdata.split("[RECEIVED]", 1)[1]
    tokens = content.split(",")

    # datetime
    dt = datetime.datetime.strptime(tokens[0][3:-1], "%a %b %d %H:%M:%S %Y").strftime("%Y-%m-%d %H:%M:%S")
    line += dt
    line += Globals.CSV_SEPARATOR
    # latitude
    line += tokens[1][2:]
    line += Globals.CSV_SEPARATOR
    # longitude
    line += tokens[2][1:-1]
    line += Globals.CSV_SEPARATOR
    # accuracy (horizontal)
    accuracy = tokens[3].split("accu = ", 1)[1]
    line += accuracy
    line += Globals.CSV_SEPARATOR
    # heading
    head = tokens[4].split("head = ", 1)[1]
    line += head
    line += Globals.CSV_SEPARATOR
    # speed
    speed = tokens[5].split("speed = ", 1)[1]
    line += speed
    line += Globals.CSV_SEPARATOR
    # tstamp
    ts = int(tokens[6].split("tstamp = ", 1)[1][:-1])
    tstamp = datetime.datetime.utcfromtimestamp(ts / 1000).strftime('%Y-%m-%d %H:%M:%S')
    line += tstamp

    line += "\n"
    return line


# 730 2018/11/21 13:55:27.807121 49.6573 6 3xx Navi /GPS 492 log warn verbose 1 /GPSClientPrivate.cpp@230
# [492:1995445328] onPositionChangedSignal() GPS position 1 [REJECTED], filtered
def handle_rejected(gpsdata):
    line = Globals.TAG_DEVICE_BUI350
    line += Globals.CSV_SEPARATOR

    line += get_position_id(gpsdata)
    line += Globals.CSV_SEPARATOR

    line += Globals.TAG_REJECTED
    line += "\n"
    return line


# 734 2018/11/21 13:55:28.228216 50.0776 10 3xx Navi /GPS 492 log warn verbose 1 /GPSClientPrivate.cpp@214
# [492:1995445328] onPositionChangedSignal() GPS position 3 [ACCEPTED]
def handle_accepted(gpsdata):
    line = Globals.TAG_DEVICE_BUI350
    line += Globals.CSV_SEPARATOR

    line += get_position_id(gpsdata)
    line += Globals.CSV_SEPARATOR

    line += Globals.TAG_ACCEPTED
    line += "\n"
    return line


# 747 2018/11/21 13:55:29.425294 51.2754 7 3xx Navi /Sko 492 log warn verbose 1 /SkobblerService.cpp@791
# [492:1933571152] OnNewPosition() GPS position 7 [PROCESSED] @ Thu Sep 17 10:03:02 2015
def handle_processed(gpsdata):
    line = Globals.TAG_DEVICE_BUI350
    line += Globals.CSV_SEPARATOR

    line += get_position_id(gpsdata)
    line += Globals.CSV_SEPARATOR

    line += Globals.TAG_PROCESSED
    line += Globals.CSV_SEPARATOR

    content = gpsdata.split("[PROCESSED]", 1)[1]
    tokens = content.split(",")

    dt = datetime.datetime.strptime(tokens[0][3:-1], "%a %b %d %H:%M:%S %Y").strftime("%Y-%m-%d %H:%M:%S")
    line += dt
    line += "\n"
    return line


# 4944 2019/03/28 09:22:06.613677 341.5449 26 3xx Navi /Eve 1901 log warn verbose 1 /EventLoopWayland.cpp@261
# [1901:1933571152] NaviHandleEvents() GPS position [MATCHED] @ Thu Mar 28 08:21:53 2019 : (48.787129, 8.923630),
# heading = 156.60

def handle_matched(gpsdata):
    # device
    line = Globals.TAG_DEVICE_BUI350
    line += Globals.CSV_SEPARATOR
    # position id
    # N/A
    line += Globals.CSV_SEPARATOR
    # status
    line += Globals.TAG_MATCHED
    line += Globals.CSV_SEPARATOR

    index1 = gpsdata.rfind(":", 0)
    index2 = gpsdata.rfind("@", 0)

    if index1 != -1 and index2 != -1:
        dtstr = gpsdata[index2 + 2:index1-1]
        dt = datetime.datetime.strptime(dtstr, "%a %b %d %H:%M:%S %Y").strftime("%Y-%m-%d %H:%M:%S")
        # datetime
        line += dt
        line += Globals.CSV_SEPARATOR

        coord_start_idx = gpsdata.rfind("(", index1)
        coord_end_idx = gpsdata.rfind(")", index1)

        coord = gpsdata[coord_start_idx + 1:coord_end_idx]
        tokens = coord.split(",")

        # latitude
        line += tokens[0]
        line += Globals.CSV_SEPARATOR
        # longitude
        line += tokens[1][1:]
    else:
        line += Globals.CSV_SEPARATOR
        line += Globals.CSV_SEPARATOR

    line += "\n"
    return line


def parse(filename):
    with open(filename, 'r') as fIn:
        csvfilename = filename.replace(Globals.BUI350_LOG_FILENAME, Globals.BUI350_GPS_FILENAME)
        with open(csvfilename, 'w') as fOut:
            for inLine in fIn:
                if "[RECEIVED]" in inLine:
                    out = handle_received(inLine)
                    fOut.write(out)
                elif "[REJECTED]" in inLine:
                    out = handle_rejected(inLine)
                    fOut.write(out)
                elif "[ACCEPTED]" in inLine:
                    out = handle_accepted(inLine)
                    fOut.write(out)
                elif "[PROCESSED]" in inLine:
                    out = handle_processed(inLine)
                    fOut.write(out)
                elif "[MATCHED]" in inLine:
                    out = handle_matched(inLine)
                    fOut.write(out)
                else:
                    continue
