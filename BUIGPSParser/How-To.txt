How to get map road data for a region of interest:
===================================================

1. Download .shp.zip files for the region of interest from http://download.geofabrik.de/
2. Import the shape data into PostgreSQL:

	createdb -U USERNAME sweden
	psql -U USERNAME sweden -c "CREATE EXTENSION postgis"

	shp2pgsql -s 4326 gis_osm_roads_free_1.shp roads | psql -U USERNAME sweden

3. Create a new shapefile containing the roads within the area of interest defined by a bounding box:

	pgsql2shp -f lund -h localhost -u USERNAME -P PASSWORD sweden "SELECT osm_id, geom FROM roads WHERE ST_Within(geom, ST_SetSRID(ST_GeomFromText('POLYGON((13.17 55.60, 13.17 55.74, 13.35 55.74, 13.35 55.60, 13.17 55.60))'), 4326));"

4. Copy the resulting files (usually 5) in the "maps" folder
